#include <unistd.h>
#include <stdio.h>
#include <zmq.h>
#include <assert.h>
#include <mqueue.h>

void rover_data_subscribe(void)
{
    printf("ZMQ publishing data...\n");
    void *context = zmq_ctx_new();
    void *publisher = zmq_socket(context, ZMQ_PUB);
    int rc = zmq_bind(publisher, "tcp://*:8123");
    assert(rc == 0);


    //float vel = 0.0045f;
    float vel = 0.5000f;

    while (1)
    {
        printf(".");
        
        zmq_msg_t msg;
        rc = zmq_msg_init_size(&msg, 4);
        assert (rc == 0);

        void *d = zmq_msg_data(&msg);
        *(float *)d = vel;

        rc = zmq_msg_send(&msg, publisher, 0);
        assert(rc > 0);
        printf("sending vel:%f\n", vel);

        // vel += 0.1f;
        usleep(100000);
    }

    zmq_close(publisher);
    zmq_ctx_destroy(context);

}

int main(){
    rover_data_subscribe();
}
