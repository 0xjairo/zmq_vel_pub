#include <stdio.h>
#include <zmq.h>
#include <assert.h>
#include <mqueue.h>

void rover_data_subscribe(void)
{
    printf("ZMQ subscribing data...\n");
    void *context = zmq_ctx_new();
    void *subscriber = zmq_socket(context, ZMQ_SUB);

    int rc;

    rc = zmq_setsockopt(subscriber, ZMQ_SUBSCRIBE, "", 0);
    assert(rc == 0);

    int flag = 0;
    rc = zmq_setsockopt(subscriber, ZMQ_IPV6, &flag, sizeof(int));
    assert(rc == 0);

    rc = zmq_connect(subscriber, "tcp://10.0.3.136:8123");
    assert(rc == 0);

    zmq_msg_t msg;
    rc = zmq_msg_init(&msg);
    assert (rc == 0);

    while (1)
    {
        rc = zmq_msg_recv(&msg, subscriber, 0);
        printf("new data\n");
        if(rc != 4){
            continue;
        }
        void *d = zmq_msg_data(&msg);
        printf("new data value: %f\n", *(float *)d);
    }

    zmq_close(subscriber);
    zmq_ctx_destroy(context);

}

int main(){
    rover_data_subscribe();
}
